---
# General
title: "Liens"
author: "Louis-Olivier Brassard"
date: "2020"
description: "Quelques liens RSS"
lang: "fr"

# Logo
# logo-top: "<img src='template/img/logo-top.jpg'>"
logo-bottom: "lob"

# Colors
color-background: "#e5ece1"
color-static: "#0d3018"
color-dynamic: "#dd1a2a"
border-width: "4px"

# Links
links:
- text: "<strong>Antoine Fauchié</strong><br>Curieux veilleur"
  url: "https://www.quaternum.net/atom.xml"
- text: "<strong>Florens Verscheld</strong><br>Développement, design, et un peu plus."
  url: "https://fvsch.com/feed"
- text: "<strong>Max Böck</strong><br>IndieWeb practician, ethical developer."
  url: "https://mxb.dev/feed.xml"
- text: "<strong>David Larlet</strong><br>Espace personnel de David Larlet."
  url: "https://larlet.fr/david/log/"
#- text: ""
#  url: ""
---
